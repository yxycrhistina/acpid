Name: acpid
Version: 2.0.32
Release: 1
Summary: Advanced Configuration and Power Interface event daemon
License: GPLv2+
URL: http://sourceforge.net/projects/acpid2/
Source0: http://downloads.sourceforge.net/acpid2/%{name}-%{version}.tar.xz
Source1: acpid.power.conf
Source2: acpid.power.sh
Source3: acpid.service
Source4: acpid.sysconfig
Source5: acpid.socket

Patch1: acpid-2.0.32-kacpimon-dynamic-connections.patch
ExclusiveArch: aarch64 x86_64 %{ix86} riscv64

BuildRequires: systemd, gcc
Requires: systemd

%description
acpid is designed to notify user-space programs of ACPI events.

%package help
Summary:  Development documents for ACPI library
Requires: %{name} = %{version}-%{release}

%description help
Development document for ACPI library.

%prep
%autosetup -n %{name}-%{version} -p1


%build
%configure
%make_build CFLAGS="%{optflags} -pie -Wl,-z,relro,-z,now"

%install
rm -rf %{buildroot}/*
mkdir -p -m 755 %{buildroot}%{_sysconfdir}/acpi/events
mkdir -p -m 755 %{buildroot}%{_sysconfdir}/acpi/actions
mkdir -p -m 755 %{buildroot}%{_unitdir}
mkdir -p -m 755 %{buildroot}%{_sysconfdir}/sysconfig
%make_install docdir=%{_docdir}/%{name}
install -p -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/acpi/events/powerconf
install -p -m 755 %{SOURCE2} %{buildroot}%{_sysconfdir}/acpi/actions/power.sh
install -p -m 644 %{SOURCE3} %{SOURCE5} %{buildroot}%{_unitdir}
install -p -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/acpid

%files
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}.socket
%dir %{_sysconfdir}/acpi
%dir %{_sysconfdir}/acpi/events
%dir %{_sysconfdir}/acpi/actions
%config %attr(0644,root,root) %{_sysconfdir}/acpi/events/powerconf
%config %attr(0755,root,root) %{_sysconfdir}/acpi/actions/power.sh
%config %attr(0644,root,root) %{_sysconfdir}/sysconfig/acpid
%{_bindir}/acpi_listen
%{_sbindir}/acpid
%{_sbindir}/kacpimon

%files help
%doc %{_docdir}/%{name}
%{_mandir}/man8/acpid.8.gz
%{_mandir}/man8/acpi_listen.8.gz
%{_mandir}/man8/kacpimon.8.gz

%post
%systemd_post %{name}.socket %{name}.service

%preun
%systemd_preun %{name}.socket %{name}.service

%postun
%systemd_postun_with_restart %{name}.socket %{name}.service

%changelog
* Thu Jul 23 2020 jinzhimin <jinzhimin2@huawei.com> - 2.0.32-1
- update version to 2.0.32

* Tue Jan 21 2020 luochunsheng <luochunsheng@huawei.com> - 2.0.30-4
- fix acpid.service start failed: Mishandling the acpid sysconfig file 
  as a directory

* Fri Jan 10 2020 wuxu_wu <wuxu.wu@huawei.com> - 2.0.30-3
- delete useless configuration file

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.30-2
- Package init

